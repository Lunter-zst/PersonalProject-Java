package week2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws Exception {
		Map<String, Integer> map = new TreeMap<String,Integer>();
		Scanner in = new Scanner(System.in);
		System.out.println("please input path:");
		String path = in.next();
		int countChar = 0;
		int countword = 0;
		int countline = 0;
		InputStreamReader isr = new InputStreamReader(new FileInputStream(path));
		BufferedReader br = new BufferedReader(isr);
		String s = null;
		while((s = br.readLine())!=null)
		{
			countChar += s.length()+1;
			if(!s.equals(""))
			{
				String a = s.toLowerCase();
				String[] str = a.split("[������������������'\"\t,.;?! ]");
				for (int i = 0; i < str.length; i++) 
				{
					if(!str[i].equals(""))
					{
						char[] temp = str[i].toCharArray();
						if(Character.isDigit(temp[0]))
							break;
						else
						{
							if(map.get(str[i])==null)
								map.put(str[i],new Integer(1));
							else
							{
								map.put(str[i],map.get(str[i]).intValue()+1);
							}
							countword++;
						}
					}
				}
				countline++;
			}
		}
		isr.close();
		System.out.println("characters:"+countChar);
		System.out.println("words:"+countword );
		System.out.println("lines:"+countline);
		
		List<Map.Entry<String,Integer>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list,(o1, o2) -> {
			if(o1.getValue()!=o2.getValue())
				return o2.getValue() - o1.getValue();
			return o1.getKey().toString().compareTo(o2.getKey());
		});
		int x=0;
		for(Map.Entry<String,Integer> entry:list)
		{
			if(x<10)
			{
				System.out.println("<"+entry.getKey()+">:"+entry.getValue());
				x++;
			}
		}
	}
}
