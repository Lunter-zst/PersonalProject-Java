package week3;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Wordgroup {
	Map<String, Integer> map = new TreeMap<String,Integer>();
	
	public Map<String, Integer> getMap() {
		return map;
	}

	public void setMap(Map<String, Integer> map) {
		this.map = map;
	}

	public void worldgroup(List<String> list,int len) 
	{
		for(int i=0;i<list.size();i++){
			if(list.get(i).equals(""))
				list.remove(i); 
        }
		for (int i = 0; i < list.size()-len+1; i++) {
			String temp = list.get(i);
			for (int j = i+1; j < i+len; j++) {
				temp = temp+" "+list.get(j);
			}
			if(map.get(temp)==null)
				map.put(temp,new Integer(1));
			else
			{
				map.put(temp,map.get(temp).intValue()+1);
			}
		}
	}
}
