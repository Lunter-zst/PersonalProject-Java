package worldCount;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Read {
	Map<String, Integer> map = new TreeMap<String,Integer>();
	private int countChar = 0;
	private int countword = 0;
	private int countline = 0;
	private List<String> list = new ArrayList<String>();

	public int getCountChar() {
		return countChar;
	}

	public void setCountChar(int countChar) {
		this.countChar = countChar;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public int getCountword() {
		return countword;
	}

	public void setCountword(int countword) {
		this.countword = countword;
	}

	public int getCountline() {
		return countline;
	}

	public void setCountline(int countline) {
		this.countline = countline;
	}
	
	public Map<String, Integer> getMap() {
		return map;
	}

	public void setMap(Map<String, Integer> map) {
		this.map = map;
	}

	public void countNum(String path) throws IOException
	{
		File file=new File(path); 
		if(file.exists())
		{
			InputStreamReader isr = new InputStreamReader(new FileInputStream(path));
			BufferedReader br = new BufferedReader(isr);
			String s = null;
			while((s = br.readLine())!=null)
			{
				countChar += s.length()+1;
				if(!s.equals(""))
				{
					String a1 = s.toLowerCase();
					String[] str = a1.split("[������������������'\"\t,.;?! ]");
					for(int i=0;i<str.length;i++){  
			            list.add(str[i]);  
			        } 
					for (int i = 0; i < str.length; i++) 
					{
						if(!str[i].equals(""))
						{
							char[] temp = str[i].toCharArray();
							if(Character.isDigit(temp[0]))
								break;
							else
							{
								if(map.get(str[i])==null)
									map.put(str[i],new Integer(1));
								else
								{
									map.put(str[i],map.get(str[i]).intValue()+1);
								}
								countword++;
							}
						}
					}
					countline++;
				}
			}
		}
		else
		{
			System.out.println("�ļ�������!");
			System.exit(0);
		}
		
	}
}
