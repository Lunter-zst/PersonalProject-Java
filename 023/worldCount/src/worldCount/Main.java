package worldCount;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws IOException{	
		Scanner in = new Scanner(System.in);
		String path = null;
		int len = 0;
		int x = 0;
		String out = null;
		String[] a = in.nextLine().split(" ");
		for (int i = 0; i < a.length-1; i+=2) {
			if(a[i].equals("-i"))
				path = a[i+1];
			else if(a[i].equals("-m"))
			{
				if(Character.isDigit(a[i+1].charAt(0)))
				{
					len = Integer.parseInt(a[i+1]);
				}
				else
				{
					System.out.println("-m的参数必须为数字!");
					System.exit(0);
				}
			}
			else if(a[i].equals("-n"))
			{
				if(Character.isDigit(a[i+1].charAt(0)))
				{
					x = Integer.parseInt(a[i+1]);
				}
				else
				{
					System.out.println("-n的参数必须为数字!");
					System.exit(0);
				}
			}
			else if(a[i].equals("-o"))
				out = a[i+1];
			else
			{
				System.out.println("参数出错!");
				System.exit(0);
			}
		}
		Read fun = new Read();
		fun.countNum(path);
		System.out.println("characters:"+fun.getCountChar());
		System.out.println("words:"+fun.getCountword());
		System.out.println("lines:"+fun.getCountline());
		
		if(x!=0)
		{
			Sort sort = new Sort();
			int x1=0;
			for(Map.Entry<String,Integer> entry:sort.sortout(fun.getMap()))
			{
				if(x1<x)
				{
					System.out.println("<"+entry.getKey()+">:"+entry.getValue());
					x1++;
				}
			}
		}
		
		Wordgroup worldgroup = new Wordgroup();
		if(len!=0)
		{
			worldgroup.worldgroup(fun.getList(),len);
			Sort sort = new Sort();
			int x1 = 0;
			for(Map.Entry<String,Integer> entry:sort.sortout(worldgroup.getMap()))
			{
				if(x1<10)
				{
					System.out.println("<"+entry.getKey()+">:"+entry.getValue());
					x1++;
				}
			}
			
		}
		
		if(out!=null)
		{
			Write write = new Write();
			write.writeout(fun.getCountChar(),fun.getCountword(),fun.getCountline(),fun.getMap(),x,out,worldgroup.getMap(),len);
		}
	}
}
