package worldCount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Sort {
	public List<Map.Entry<String,Integer>> sortout(Map<String, Integer> map){
		List<Map.Entry<String,Integer>> list = new ArrayList<>(map.entrySet());
		Collections.sort(list,(o1, o2) -> {
			if(o1.getValue()!=o2.getValue())
				return o2.getValue() - o1.getValue();
			return o1.getKey().toString().compareTo(o2.getKey());
		});
		return list;	
	}
}
